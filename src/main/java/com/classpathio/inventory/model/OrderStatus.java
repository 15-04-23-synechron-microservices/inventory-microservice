package com.classpathio.inventory.model;

public enum OrderStatus {
	
	ORDER_PLACED,
	
	ORDER_FULFILLED,
	
	ORDER_CANCELLED

}
