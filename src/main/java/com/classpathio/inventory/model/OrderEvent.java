package com.classpathio.inventory.model;

import java.time.LocalDateTime;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@ToString
public class OrderEvent {
	private Order order;
	private LocalDateTime timestamp;
	private OrderStatus status;
}
