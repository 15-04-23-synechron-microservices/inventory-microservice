package com.classpathio.inventory.service;

import java.util.function.Consumer;

import org.springframework.context.annotation.Bean;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Service;

import com.classpathio.inventory.model.OrderEvent;

@Service
public class OrderServiceProcessor {
	
	@Bean
	public Consumer<Message<OrderEvent>> consumer() {
		System.out.println("Processing the orders:: ");
		return message -> System.out.println("Consuming the order message :: "+ message);
	}

}
