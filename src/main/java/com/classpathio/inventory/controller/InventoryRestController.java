package com.classpathio.inventory.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/inventory")
public class InventoryRestController {
	
	private static int counter = 1000;
	
	@PostMapping
	public int updateInventory() {
		return --counter;
	}
	
	@GetMapping
	public int fetchInventory() {
		return counter;
	}
	

}
